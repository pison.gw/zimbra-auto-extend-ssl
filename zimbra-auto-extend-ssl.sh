#!/bin/bash
sudo -u zimbra /opt/zimbra/bin/zmproxyctl stop
sudo -u zimbra /opt/zimbra/bin/zmmailboxdctl stop

cd /root/letsencrypt
./letsencrypt-auto certonly --standalone -n
cd /etc/letsencrypt/live/mail.sgd.co.th
curl https://letsencrypt.org/certs/trustid-x3-root.pem.txt >> chain.pem
cp -Rf /etc/letsencrypt/live/mail.sgd.co.th/ /opt/zimbra/ssl/letsencrypt/
chown zimbra:zimbra -R /opt/zimbra/ssl/letsencrypt/

cd /opt/zimbra/ssl/letsencrypt/
sudo -u zimbra /opt/zimbra/bin/zmcertmgr verifycrt comm privkey.pem cert.pem chain.pem
sudo -u zimbra cp -a /opt/zimbra/ssl/zimbra /opt/zimbra/ssl/zimbra.$(date "+%Y%m%d")
sudo -u zimbra cp /opt/zimbra/ssl/letsencrypt/privkey.pem /opt/zimbra/ssl/zimbra/commercial/commercial.key
cd /opt/zimbra/ssl/letsencrypt/
sudo -u zimbra /opt/zimbra/bin/zmcertmgr deploycrt comm cert.pem chain.pem
sudo -u zimbra /opt/zimbra/bin/zmcontrol restart
